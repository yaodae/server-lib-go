package utils

import "reflect"

func AnythingToSlice(a interface{}) []interface{} {
	v := reflect.ValueOf(a)
	switch v.Kind() {
	case reflect.Slice, reflect.Array:
		result := make([]interface{}, v.Len())
		for i := 0; i < v.Len(); i++ {
			result[i] = v.Index(i).Interface()
		}
		return result
	case reflect.Ptr:
		value := v.Elem()
		result := make([]interface{}, value.Len())
		for i := 0; i < value.Len(); i++ {
			result[i] = value.Index(i).Interface()
		}
		return result
	default:
		panic("not supported")
	}
}
