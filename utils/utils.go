package utils

import "reflect"

func IsNil(i interface{}) bool {
	if i == nil {
		return true
	}
	vi := reflect.ValueOf(i)
	if vi.Kind() == reflect.Ptr {
		return vi.IsNil()
	}
	return false
}

func IsNotNil(i interface{}) bool {
	return !IsNil(i)
}

func AbsInt64(i int64) int64 {
	if i < 0 {
		return -i
	} else {
		return i
	}
}

func AbsInt32(i int32) int32 {
	if i < 0 {
		return -i
	} else {
		return i
	}
}

func AbsInt(i int) int {
	if i < 0 {
		return -i
	} else {
		return i
	}
}

func CopyDest(dest interface{}) interface{} {
	v := reflect.ValueOf(dest)
	switch v.Kind() {
	case reflect.Ptr:
		v1 := v.Elem()
		v2 := &v1
		return v2
	}
	return dest
}
