package utils

import "time"

// TimeLastDay 获取指定日期当月最后一天（可当作当月最大天数）
func TimeLastDay(date time.Time) int {
	currentYear, currentMonth, _ := date.Date()
	currentLocation := date.Location()

	firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)
	return lastOfMonth.Day()
}
