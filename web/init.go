package web

import (
	"gitee.com/yaodae/server-lib-go/config"
	"gitee.com/yaodae/server-lib-go/web/router"
	"github.com/gin-gonic/gin"
)

func InitGin(bindRouter func(r *gin.Engine)) {
	startGin(bindRouter)
}

// 核心启动gin框架函数，主函数
func startGin(bindRouter func(r *gin.Engine)) {
	// 初始化基础配置
	r := gin.Default()
	// 初始化网关
	router.InitRouter(r)
	// 绑定路由
	bindRouter(r)
	_ = r.Run(":" + config.SysConfig.HttpPort)
}
