package rocketmq

import (
	mqHttpSdk "github.com/aliyunmq/mq-http-go-sdk"
)

var (
	producers map[string]mqHttpSdk.MQProducer
	consumers map[string]mqHttpSdk.MQConsumer
	client    mqHttpSdk.MQClient
)

func InitMq(mqConf *MqConfig) {
	client = mqHttpSdk.NewAliyunMQClient(mqConf.Endpoint, mqConf.AccessKey, mqConf.SecretKey, "")
	producers = make(map[string]mqHttpSdk.MQProducer, 0)
	consumers = make(map[string]mqHttpSdk.MQConsumer, 0)
}

func InitProducer(config *MqProducerConfig) {
	if config == nil {
		return
	}
	producers[config.Name] = client.GetProducer(config.InstanceId, config.Topic)
}

func InitConsumer(config *MqConsumerConfig) {
	if config == nil {
		return
	}
	consumers[config.Name] = client.GetConsumer(config.InstanceId, config.Topic, config.Group, "")
}

func GetProducer(name string) mqHttpSdk.MQProducer {
	if cli, ok := producers[name]; ok {
		return cli
	} else {
		return nil
	}
}

func GetConsumer(name string) mqHttpSdk.MQConsumer {
	if cli, ok := consumers[name]; ok {
		return cli
	} else {
		return nil
	}
}
