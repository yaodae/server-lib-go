package rocketmq

type MqMsg struct {
	MessageBody string            // 消息内容
	MessageTag  string            // 消息标签
	Properties  map[string]string // 消息属性
	MessageKey  string            // 消息KEY
}

type MqConfig struct {
	Endpoint  string // 设置HTTP接入域名（此处以公共云生产环境为例）
	AccessKey string // AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
	SecretKey string // SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
}

type MqProducerConfig struct {
	Name       string // 生产者名字
	Topic      string // 所属的 Topic
	InstanceId string // Topic所属实例ID，默认实例为空
}

type MqConsumerConfig struct {
	Name       string // 客户端名字
	Topic      string // 所属的 Topic
	Group      string // 您在控制台创建的 Consumer ID(Group ID)
	InstanceId string // Topic所属实例ID，默认实例为空
}
