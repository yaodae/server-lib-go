package mysql

type DbDaoMysqlSupport struct {
}

func (db DbDaoMysqlSupport) SelectOne(query interface{}) interface{} {
	return nil
}

func (db DbDaoMysqlSupport) SelectSortPageList(query interface{}, skip, limit int, sort ...string) interface{} {
	return nil
}

func (db DbDaoMysqlSupport) CountList(query interface{}) uint64 {
	return 0
}

func (db DbDaoMysqlSupport) InsertOne(dest interface{}) int64 {
	return MAdd(dest)
}

func (db DbDaoMysqlSupport) UpdateOne(dest interface{}, query interface{}, update interface{}) int64 {
	return MSave(dest)
}

func (db DbDaoMysqlSupport) DelData(query interface{}) int64 {
	return MRemove(query)
}
