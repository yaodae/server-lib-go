package mongo

type DbDaoMongoSupport struct {
}

func (db DbDaoMongoSupport) SelectOne(query interface{}) interface{} {
	return nil
}

func (db DbDaoMongoSupport) SelectList(query interface{}, skip, limit int, sort ...string) interface{} {
	return nil
}

func (db DbDaoMongoSupport) CountList(query interface{}) uint64 {
	return 0
}

func (db DbDaoMongoSupport) InsertOne(dest interface{}) int64 {
	return 0
}

func (db DbDaoMongoSupport) UpdateOne(dest interface{}) int64 {
	return 0
}

func (db DbDaoMongoSupport) UpdateList(query interface{}, update interface{}) int64 {
	return 0
}

func (db DbDaoMongoSupport) DelData(query interface{}) int64 {
	return 0
}
