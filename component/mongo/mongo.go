package mongo

/**
 * @Title  mongo操作层
 * @Description mongo操作的封装
 * @Author yaoweixin
 * @Update 2022/2/16 10:08
 */

import (
	"gitee.com/yaodae/server-lib-go/config"
	"gitee.com/yaodae/server-lib-go/log"
	"gopkg.in/mgo.v2/bson"
)

func Init(cfg *config.MongoDbConfig) bool {
	if cfg == nil {
		return true
	}
	mongoConfig := MgServerConfig{Name: cfg.MongoName, Hosts: cfg.MongoHosts, AuthDb: cfg.MongoAuthDb,
		AuthUser: cfg.MongoAuthUser, AuthPass: cfg.MongoAuthPass, MaxPoolSize: cfg.MongoMaxPoolSize}
	initMongo(mongoConfig)
	return true
}

func Release() {
	closeDb()
}

type Query struct {
	Key   string
	value string
}

func GetDb() string {
	return "default"
}

// MFindOne 查找一个数据
func MFindOne(db, collection string, selector bson.M, dest interface{}) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	err := session.DB(db).C(collection).Find(selector).One(dest)
	if err != nil {
		return false
	} else {
		return true
	}
}

// MCount 查找数据数量
func MCount(db, collection string, selector bson.M) uint64 {
	session := getConn(GetDb())
	defer releaseConn(session)
	num, err := session.DB(db).C(collection).Find(selector).Count()
	if err != nil {
		return 0
	} else {
		return uint64(num)
	}
}

// MFindList 查找数据列表
func MFindList(db, collection string, selector bson.M, destList interface{}) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	iter := session.DB(db).C(collection).Find(selector).Iter()
	_ = iter.All(destList)
	return true
}

// MFindPageList 分页查找数据列表
func MFindPageList(db, collection string, skip, limit int, selector bson.M, destList interface{}, sort ...string) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	iter := session.DB(db).C(collection).Find(selector).Sort(sort...).Skip(skip).Limit(limit).Iter()
	_ = iter.All(destList)
	return true
}

// MFindSortPageList 分页查找数据列表
func MFindSortPageList(db, collection string, skip, limit int, selector bson.M, destList interface{}, sort ...string) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	iter := session.DB(db).C(collection).Find(selector).Sort(sort...).Skip(skip).Limit(limit).Iter()
	_ = iter.All(destList)
	return true
}

// MAdd 插入数据
func MAdd(db, collection string, dest interface{}) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	err := session.DB(db).C(collection).Insert(dest)
	if err != nil {
		log.Error("mongo add error================", err.Error())
		return false
	} else {
		return true
	}
}

// MSave 更新数据
func MSave(db, collection string, selector, update bson.M) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	err := session.DB(db).C(collection).Update(selector, update)
	if err != nil {
		log.Error("mongo save error================", err.Error())
		return false
	} else {
		return true
	}
}

// MRemove 删除数据
func MRemove(db, collection string, selector bson.M) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	err := session.DB(db).C(collection).Remove(selector)
	if err != nil {
		log.Error("mongo remove error================", err.Error())
		return false
	} else {
		return true
	}
}

// MUpdate 更新数据
func MUpdate(db, collection string, selector, update bson.M) bool {
	session := getConn(GetDb())
	defer releaseConn(session)
	_, err := session.DB(db).C(collection).UpdateAll(selector, update)
	if err != nil {
		log.Error("mongo update error================", err.Error())
		return false
	} else {
		return true
	}
}
