package redis

import "time"

const gameDb = "server"

func RDel(key string) {
	Util.del(gameDb, key)
}

func RExists(key string) bool {
	return Util.exists(gameDb, key)
}

func RExpire(key string, time int) {
	Util.expire(gameDb, key, time)
}

func RGet(key string) string {
	return Util.get(gameDb, key)
}

func RGetAny(keys ...string) []string {
	return Util.getAny(gameDb, keys...)
}

func RSet(key string, value string) {
	Util.set(gameDb, key, value)
}

func RSetEX(key string, value string, expire int) {
	Util.setEX(gameDb, key, value, expire)
}

func RSetNX(key string, value string) bool {
	return Util.setNX(gameDb, key, value)
}

func RSetNxAndEx(key string, value string, expire int) bool {
	return Util.setNxAndEx(gameDb, key, value, expire)
}

func RIncr(key string) {
	RIncrBy(key, 1)
}

func RIncrBy(key string, num int) {
	Util.incrBy(gameDb, key, num)
}

func RIncrByValue(key string, num int) int64 {
	return Util.incrByValue(gameDb, key, num)
}

func RHDel(key string, field string) {
	Util.hDel(gameDb, key, field)
}

func RHExists(key string, field string) bool {
	return Util.hExists(gameDb, key, field)
}

func RHGet(key string, field string) string {
	return Util.hGet(gameDb, key, field)
}

func RHSet(key string, field string, value string) {
	Util.hSet(gameDb, key, field, value)
}

func RHSetEx(key string, field string, value string, expire int) {
	Util.hSetEx(gameDb, key, field, value, expire)
}

func RHSetNX(key string, field string, value string) bool {
	return Util.hSetNX(gameDb, key, field, value)
}

func RHGetAll(key string) map[string]string {
	return Util.hGetAll(gameDb, key)
}

func RHIncrBy(key string, field string, num int) {
	Util.hIncrBy(gameDb, key, field, num)
}

func RHIncrByEx(key string, field string, num int, expire int) {
	Util.hIncrByEx(gameDb, key, field, num, expire)
}

func RHLen(key string) int64 {
	return Util.hLen(gameDb, key)
}

func RHMGet(key string, field ...string) []string {
	return Util.hMGet(gameDb, key, field...)
}

func RHMSet(key string, fieldValue ...string) {
	Util.hMSet(gameDb, key, fieldValue...)
}

func RHMSetEx(key string, expire int, fieldValue ...string) {
	Util.hMSetEx(gameDb, key, expire, fieldValue...)
}

func RHValues(key string) []string {
	return Util.hValues(gameDb, key)
}

func RLLen(key string) int64 {
	return Util.lLen(gameDb, key)
}

func RLLPop(key string) string {
	return Util.lLPop(gameDb, key)
}

func RLLPush(key string, value ...string) {
	Util.lLPush(gameDb, key, value...)
}

func RLRPop(key string) string {
	return Util.lRPop(gameDb, key)
}

func RLRPush(key string, value ...string) {
	Util.lRPush(gameDb, key, value...)
}

func RLRange(key string, start, end int) []string {
	return Util.lRange(gameDb, key, start, end)
}

func RSAdd(key string, value ...string) {
	Util.sAdd(gameDb, key, value...)
}

func RSAddEx(key string, expire int, value ...string) {
	Util.sAddEx(gameDb, key, expire, value...)
}

func RSCard(key string) int64 {
	return Util.sCard(gameDb, key)
}

func RSRem(key string, value ...string) {
	Util.sRem(gameDb, key, value...)
}

func RSMembers(key string) []string {
	return Util.sMembers(gameDb, key)
}

func RZAdd(key string, member string, score float64) {
	Util.zAdd(gameDb, key, member, score)
}

func RZAddEx(key string, member string, score float64, expire int) {
	Util.zAddEx(gameDb, key, member, score, expire)
}

func RZAdds(key string, arg ...interface{}) {
	Util.zAdds(gameDb, key, arg...)
}

func RZAddsEx(key string, expire int, arg ...interface{}) {
	Util.zAddsEx(gameDb, key, expire, arg...)
}

func RZCard(key string) int64 {
	return Util.zCard(gameDb, key)
}

func RZIncrBy(key string, member string, score float64) {
	Util.zIncrBy(gameDb, key, member, score)
}

func RZRange(key string, start, end int, withScore, isRev bool) [][]string {
	return Util.zRange(gameDb, key, start, end, withScore, isRev)
}

func RZRangeByScore(key string, min, max float64, withScore, isRev bool) [][]string {
	return RZRangeByScoreLimit(key, min, max, withScore, isRev, -1, -1)
}

func RZRangeByScoreLimit(key string, min, max float64, withScore, isRev bool, offset, count int) [][]string {
	return Util.zRangeByScoreLimit(gameDb, key, min, max, withScore, isRev, offset, count)
}

// RZRank 0为开始
func RZRank(key string, member string, isRev bool) (int64, error) {
	return Util.zRank(gameDb, key, member, isRev)
}

func RZRem(key string, member ...string) {
	Util.zRem(gameDb, key, member...)
}

func RZRemByRank(key string, start, stop int64) (int64, error) {
	return Util.zRemByRank(gameDb, key, start, stop)
}

func RZScore(key string, member string) (float64, error) {
	return Util.zScore(gameDb, key, member)
}

func REval(script string, keys []string, args ...interface{}) (interface{}, error) {
	return Util.eval(gameDb, script, keys, args...)
}

func RTtl(key string) (time.Duration, error) {
	return Util.ttl(gameDb, key)
}
