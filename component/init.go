package component

import (
	"fmt"
	"gitee.com/yaodae/server-lib-go/component/mongo"
	"gitee.com/yaodae/server-lib-go/component/mysql"
	"gitee.com/yaodae/server-lib-go/component/redis"
	"gitee.com/yaodae/server-lib-go/component/rocketmq"
	"gitee.com/yaodae/server-lib-go/config"
)

func InitComponent() error {
	// 加载mysql
	if !mysql.Init(config.SysConfig.MysqlCfg) {
		return fmt.Errorf("发生了一个致命错误，程序结束 \n")
	}
	// 加载mongo
	if !mongo.Init(config.SysConfig.MongoCfg) {
		return fmt.Errorf("发生了一个致命错误，程序结束 \n")
	}
	// 加载redis
	if !redis.Init(config.SysConfig.RedisCfg) {
		return fmt.Errorf("发生了一个致命错误，程序结束 \n")
	}
	// 加载RocketMq
	if !rocketmq.Init(config.SysConfig.RocketmqCfg) {
		return fmt.Errorf("发生了一个致命错误，程序结束 \n")
	}
	// 组件加载正常，可以继续向下进行
	return nil
}
