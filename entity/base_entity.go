package entity

import (
	"time"
)

type Entity interface {
	IsEmpty() bool
	CacheKey() string
	CacheOpen() bool
}

type AbstractEntity struct {
	ID         uint
	ObjectId   string
	CreateTime time.Time // 创建时间
	UpdateTime time.Time // 更新时间
	Delete     bool      // 是否删除
	Created    bool      // 是否已创建
}

func (s AbstractEntity) IsEmpty() bool {
	return s.ObjectId == "" && !s.Created
}

func (s *AbstractEntity) OnCreate() {
	s.CreateTime = time.Now()
	s.UpdateTime = time.Now()
	s.Delete = false
	s.Created = false
}

func (s *AbstractEntity) OnModify() {
	s.UpdateTime = time.Now()
}
