package query

type SelectParam struct {
	Query interface{}
	Sort  string
	Skip  int
	Limit int
}

func NewSelectParam(queryParam interface{}) SelectParam {
	return SelectParam{Query: queryParam}
}

func NewPageSelectParam(queryParam interface{}, sort string, skip, limit int) SelectParam {
	if skip < 0 {
		skip = 0
	}
	if limit < 1 {
		limit = 1
	}
	if limit > 10000 {
		limit = 10000
	}
	return SelectParam{Query: queryParam, Sort: sort, Skip: skip, Limit: limit}
}
