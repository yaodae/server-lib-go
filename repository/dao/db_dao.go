package dao

type DbDao interface {
	SelectOne(query interface{}) interface{}
	SelectList(query interface{}, skip, limit int, sort ...string) interface{}
	CountList(query interface{}) uint64
	InsertOne(dest interface{}) int64
	UpdateOne(dest interface{}) int64
	UpdateList(query interface{}, update interface{}) int64
	DelData(query interface{}) int64
}
