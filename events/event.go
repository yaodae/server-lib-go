package events

type EventName string

const (
	EventNone EventName = "none" // 未知
)

// Event 事件结构体
type Event struct {
	Name  EventName   // 事件名
	Value interface{} // 事件值
}

// EventListener 事件监听器结构体
type EventListener struct {
	name EventName                           // 事件名
	f    func(workId int, value interface{}) // 事件执行方法
}

// Execute 执行事件监听器的方法
func (t *EventListener) Execute(workId int, value interface{}) {
	t.f(workId, value) //调用任务所绑定的函数
}
