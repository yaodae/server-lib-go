package common

// Minute1Second one minute second
const Minute1Second = 60

// Minute15Second 15 minute second
const Minute15Second = Minute1Second * 15

// Hour1Second one hour second
const Hour1Second = Minute1Second * 60

// Day1Second one day second
const Day1Second = Hour1Second * 24

// Hour30Second 30 hour second
const Hour30Second = Hour1Second * 30

// Day3Second three day second
const Day3Second = Day1Second * 3

// Day7Second seven day second
const Day7Second = Day1Second * 7

// Day30Second 30 day second
const Day30Second = Day1Second * 30

const TimeFormat = "2006-01-02 15:04:05"

const TimeDayFormat = "2006-01-02"

const TimeMonthFormat = "2006-01"
