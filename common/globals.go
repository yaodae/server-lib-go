package common

// RetOk 一切OK的返回码
const RetOk = "0"

// RetSystemFail 系统错误的返回码
const RetSystemFail = "500"

// CodeOk 一切OK的CODE
const CodeOk = "00000"

// CodeRpcFail rpc调用异常
const CodeRpcFail = "B0001"

// MsgOk 一切OK的消息
const MsgOk = "ok"

// DbCacheModeAll 数据库缓存模式-全部
const DbCacheModeAll = "data/cache"

// DbCacheModeCacheFirst 数据库缓存模式-缓存
const DbCacheModeCacheFirst = "cache"

// DbDataUpdateQueue 数据更新队列名
const DbDataUpdateQueue = "Cache:Update:"
