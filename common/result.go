package common

import (
	"encoding/json"
)

type LResult struct {
	Api     string      `json:"api,omitempty"`
	Code    string      `json:"code,omitempty"`
	ErrCode string      `json:"err_code,omitempty"`
	Msg     string      `json:"msg,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func TestFail(lResult *LResult) bool {
	return lResult.Code != RetOk
}

func Ok() *LResult {
	return &LResult{Code: RetOk, ErrCode: CodeOk, Msg: MsgOk}
}

func OfOkData(data interface{}) *LResult {
	result := Ok()
	if data != nil {
		jsonB, _ := json.Marshal(data)
		result.Data = string(jsonB)
	}
	return result
}

func GenOkData(data interface{}) *LResult {
	result := Ok()
	result.Data = data
	return result
}

func OfFail(code string, errCode string, msg string) *LResult {
	return &LResult{Code: code, ErrCode: errCode, Msg: msg}
}

func OfFailData(code string, errCode string, msg string, data interface{}) *LResult {
	result := OfFail(code, errCode, msg)
	if data != nil {
		jsonB, _ := json.Marshal(data)
		result.Data = string(jsonB)
	}
	return result
}

func GenFailData(code string, errCode string, msg string, data interface{}) *LResult {
	result := OfFail(code, errCode, msg)
	result.Data = data
	return result
}
