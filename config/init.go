package config

import (
	"flag"
	"fmt"
	"github.com/spf13/viper"
	"strings"
)

var (
	SysConfig     ServerConfig
	SysConfigDir  string
	SysConfigName string
)

func InitConfig() error {
	flag.StringVar(&SysConfigDir, "d", "./conf/", "配置一个启动配置文件存放目录，默认：./conf/")
	flag.StringVar(&SysConfigName, "n", "sys", "配置启动配置文件名称，默认：sys")
	flag.Parse()
	// 加载启动配置
	if configLoad() {
		// 配置加载正常，可以继续向下进行
		return nil
	} else {
		return fmt.Errorf("发生了一个致命错误，程序结束 \n")
	}
}

func configLoad() bool {
	config := viper.New()
	config.AddConfigPath(SysConfigDir)
	config.SetConfigName(SysConfigName)
	config.SetConfigType("json")
	if err := config.ReadInConfig(); err != nil {
		panic(err)
	}
	SysConfig = NewConfig(config)
	return true
}

// NewConfig creates a new config with a given viper config if given
func NewConfig(configs ...*viper.Viper) ServerConfig {
	var cfg *viper.Viper
	if len(configs) > 0 {
		cfg = configs[0]
	} else {
		cfg = viper.New()
	}
	cfg.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	cfg.AutomaticEnv()
	// 直接反序列化为Struct
	var configJson ServerConfig
	if err := cfg.Unmarshal(&configJson); err != nil {
		panic(err)
	}
	return configJson
}
