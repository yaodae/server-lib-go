package config

type RocketmqConfig struct {
	MqEndpoint       string
	MqAccessKey      string
	MqSecretKey      string
	MqGroup          string
	MqInstanceId     string
	MqProducerTopics string
	MqConsumerTopics string
	MqSubscribe      string
}
