package config

type ServerConfig struct {
	MysqlCfg    *MysqlConfig    //
	MongoCfg    *MongoDbConfig  //
	RedisCfg    *RedisConfig    //
	RocketmqCfg *RocketmqConfig //
	OssCfg      *OssConfig      //
	HttpPort    string          // 监听端口
	ServiceName string          // 服务名称
	Debug       bool            // 是否调试模式
}
