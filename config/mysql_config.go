package config

type MysqlConfig struct {
	MysqlMaxIdle   int
	MysqlMaxActive int
	MysqlDb        []MysqlDbConfig
}

type MysqlDbConfig struct {
	MysqlName string
	MysqlUrl  string
	MysqlUser string
	MysqlPwd  string
	MysqlDb   string
}
