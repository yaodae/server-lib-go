package config

type MongoDbConfig struct {
	MongoName        string
	MongoHosts       string
	MongoAuthDb      string
	MongoAuthUser    string
	MongoAuthPass    string
	MongoMaxPoolSize int
}
