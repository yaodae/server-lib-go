package config

type RedisConfig struct {
	RedisMaxIdle   int
	RedisMaxActive int
	RedisTimeout   int
	RedisDb        []RedisDbConfig
}

type RedisDbConfig struct {
	RedisName string
	RedisUrl  string
	RedisPwd  string
	RedisDb   int
}
