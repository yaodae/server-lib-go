package po

import (
	"gopkg.in/mgo.v2/bson"
	"reflect"
	"strings"
)

type PO interface {
	Db() string
	Table() string
}

type AbstractMysqlPO struct {
}

type AbstractMgoPO struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	CreateTime string        `bson:"create_time"`
	UpdateTime string        `bson:"update_time"`
	Delete     bool          `bson:"is_delete"` // 是否删除
}

func (o AbstractMgoPO) Db() string {
	return ""
}

func (o AbstractMgoPO) Table() string {
	return ""
}

func UpdateM(t interface{}) bson.M {
	setParam := bson.M{}
	typeObject := reflect.TypeOf(t)
	valueObject := reflect.ValueOf(t)
	typeRef := typeObject.Elem()
	valueRef := valueObject.Elem()
	for i := 0; i < typeRef.NumField(); i++ {
		name := typeRef.Field(i).Name
		bsonTag := typeRef.Field(i).Tag.Get("bson")
		transientTag := typeRef.Field(i).Tag.Get("transient")
		if bsonTag != "" {
			name = strings.Split(bsonTag, ",")[0]
		}
		if name == "_id" || name == "create_time" {
			continue
		}
		if transientTag != "" {
			continue
		}
		setParam[strings.ToLower(name)] = valueRef.Field(i).Interface()
	}
	updateParam := bson.M{"$set": setParam}
	return updateParam
}
