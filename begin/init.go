package begin

import (
	"fmt"
	"gitee.com/yaodae/server-lib-go/component"
	"gitee.com/yaodae/server-lib-go/config"
	"gitee.com/yaodae/server-lib-go/log"
	"gitee.com/yaodae/server-lib-go/web"
	"github.com/gin-gonic/gin"
)

// Start 主配置启动函数
func Start(bindRouter func(r *gin.Engine), afterInitFunc func()) error {
	// 初始配置文件
	if err := config.InitConfig(); err != nil {
		panic(fmt.Errorf("致命错误: %s \n", err))
	}
	// 启动日志组件
	if err := log.InitLog(); err != nil {
		panic(fmt.Errorf("致命错误: %s \n", err))
	}
	// 启动组件
	if err := component.InitComponent(); err != nil {
		panic(fmt.Errorf("致命错误: %s \n", err))
	}
	// 调用初始化之后方法
	afterInitFunc()
	// 启动gin框架
	web.InitGin(bindRouter)
	return nil
}
